import React from 'react';
import './VideoItem.css';


const VideoItem = ({ yt, onVideoSelect }) =>{
    return (
        <div onClick={()=>{onVideoSelect(yt)}} className="item video-item">
            <img className='ui image' src={yt.snippet.thumbnails.medium.url} alt={yt.id.videoId} />
            <div className='content'>
                <div className="header">{yt.snippet.title}</div>
            </div>
        </div>
    );
}

export default VideoItem;