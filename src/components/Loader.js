import React from 'react';
import './Loader.css';

const Loader = () =>{
    return (
        <div className='ui segment my-loader'>
            <div className='ui active inverted dimmer'>
                <div className="ui text loader">Loading</div>
            </div>
            <p></p>
            <p></p>
            <p></p>
        </div>
    );
}

export default Loader;