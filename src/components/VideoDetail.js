import React from 'react'
import Loader from './Loader';

const VideoDetail = ({video}) =>{

    if(!video){
        return <Loader/>
    }
        const videoSrc = `https://youtube.com/embed/${video.id.videoId}`;
        return (
            <div>
                <div className="ui segment">
                <iframe 
                width="100%"
                height="400"
                src={videoSrc}
                title={video.snippet.title}
                frameBorder="0"
                allowFullScreen>
                </iframe>
                    <h4 className='header'>{video.snippet.title}</h4>
                    <p>{video.snippet.description}</p>
                </div>
            </div>
        );
}

export default VideoDetail;