import React from 'react';
import './VideoList.css';
import VideoItem from './VideoItem';

const VideoList = ({ videos, onVideoSelect }) =>{
    
    const renderedList = videos.map(video =>{
        return <VideoItem key={video.id.videoId} yt={video} onVideoSelect={onVideoSelect}/>;
    });

    return <div className='ui relaxed divided list'>{renderedList}</div>
};

export default VideoList;