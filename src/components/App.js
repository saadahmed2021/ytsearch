import React from "react";
import SearchBar from "./SearchBar";
import youtube from "../apis/youtube";
import VideoList from "./VideoList";
import VideoDetail from "./VideoDetail";
import Loader from "./Loader";

class App extends React.Component{
    
    state = {videos:[], selectedVideo: null}

    componentDidMount(){
        this.onSearchSubmit('web dev');
    }

    onSearchSubmit = async (searchText) =>{
        const response = await youtube.get('/search', {
            params:{
                q: searchText
            }
        });

        this.setState({
            videos:response.data.items,
            selectedVideo:response.data.items[0]
        })
    }

    onVideoSelect = (video) =>{
        this.setState({selectedVideo:video})
    }

    render(){
        return <div className="ui container mt-3 mb-3">
        <SearchBar onFormSubmit={this.onSearchSubmit}/>
        <div className="ui grid">
            <div className="ui row">
                <div className="eleven wide column">
                {this.state.selectedVideo===null ? <Loader/>:<VideoDetail video={this.state.selectedVideo}/>}
                </div> 
                <div className="five wide column">
                {this.state.videos.length===0 ? <Loader/>:<VideoList videos={this.state.videos} onVideoSelect={this.onVideoSelect}/>}
                </div>
            </div>
        </div>    
        </div>
    }
}

export default App;