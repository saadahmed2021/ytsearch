import React from "react";
// import ReactDOM from "react-dom";
import * as ReactDOMClient from 'react-dom/client';
import App from "./components/App";
import './index.css';

const container = document.getElementById('root')

const root = ReactDOMClient.createRoot(container)

root.render(<App/>)

// no longer support from React 18
// ReactDOM.render(<App/>,document.getElementById('root'))
