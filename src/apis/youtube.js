import axios from "axios";
const KEY = 'AIzaSyA9W8DTjOb7uut7NBmGZvNwsB5eU8ecHN8';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part:'snippet',
        maxResults:'5',
        type: 'video',
        key: KEY
    }
})

